import React from 'react'
import {connect} from 'react-redux'
import {signUserUp} from '../actions/userActions'

class SignUpComponent extends React.Component {
    state = {
        username: "",
        password: "",
        firstName:"",
        lastName: "",
        email: ""
    }

    handleOnChange = (e) => {
        e.persist();
        this.setState(() => ({
            [e.target.name]: e.target.value 
        }))
    }

    onSubmit = (e) => {
        e.preventDefault()
        this.props.signUserUp(this.state)
    }

    render(){
        return(
            <div>
                <h1>SignUp Form</h1>
                <form onSubmit={this.onSubmit}>
                    <input
                        type="lastName"
                        name="lastName"
                        placeholder="Nom de famille"
                        value={this.state.lastName}
                        onChange={this.handleOnChange}
                    />
                    <input
                        type="firstName"
                        name="firstName"
                        placeholder="Prénom"
                        value={this.state.firstName}
                        onChange={this.handleOnChange}
                    />
                    <br/>
                    <input 
                        type="text" 
                        name="username" 
                        placeholder="Username" 
                        value={this.state.username}
                        onChange={this.handleOnChange}
                    />
                    <br/>
                    <input
                        type="password"
                        name="password"
                        placeholder="Password"
                        value={this.state.password}
                        onChange={this.handleOnChange}
                    />
                    <br/>
                    <input
                        type="email"
                        name="email"
                        placeholder="Email"
                        value={this.state.email}
                        onChange={this.handleOnChange}
                    />
                    <br/>

                    <br/>
                    <input
                        type="submit"
                        value="Login"
                    />
                </form>
            </div>
        )
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        signUserUp: (userInfo) => dispatch(signUserUp(userInfo))
    }
}

export default connect(null, mapDispatchToProps)(SignUpComponent)