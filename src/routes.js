import React from 'react';

const Dashboard = React.lazy(() => import('./views/dashboard/Dashboard'));
const Formulaire = React.lazy(() => import('./views//formulaire/Formulaire'));

const routes = [
  { path: '/', exact: true, name: 'Home' },
  { path: '/dashboard', name: 'Dashboard', component: Dashboard },
  { path: '/formulaire', name: 'Formulaire', component: Formulaire },
];

export default routes;
